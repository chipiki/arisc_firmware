var structpulsgen__ch__t =
[
    [ "port", "structpulsgen__ch__t.html#a83a04ad582de2b7d36b96f9db429c2c6", null ],
    [ "pin_mask", "structpulsgen__ch__t.html#adb86b9d6ab8039a33f3e0aab67ef57a3", null ],
    [ "pin_mask_not", "structpulsgen__ch__t.html#accbd1609875ecdc8b9c0575b72e84892", null ],
    [ "pin_inverted", "structpulsgen__ch__t.html#a93dde74fef44def5407a54c88bd742f7", null ],
    [ "task", "structpulsgen__ch__t.html#ac064e347dd925fff522092558d00a490", null ],
    [ "task_infinite", "structpulsgen__ch__t.html#a75b4ea125202228ed86690ae46a778c6", null ],
    [ "task_toggles", "structpulsgen__ch__t.html#ab24b2c016c493b4c52a9fcf1abf24b07", null ],
    [ "task_toggles_todo", "structpulsgen__ch__t.html#a5000d5faa5db2fd66ff0345eefd23e6c", null ],
    [ "setup_ticks", "structpulsgen__ch__t.html#a5923fa6a48da93d9579b7d027124a322", null ],
    [ "hold_ticks", "structpulsgen__ch__t.html#a304338c045a89b201058803169dad343", null ],
    [ "todo_tick", "structpulsgen__ch__t.html#a8877063788184b18eadf986ed313ba26", null ]
];