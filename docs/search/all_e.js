var searchData=
[
  ['task',['task',['../structpulsgen__ch__t.html#ac064e347dd925fff522092558d00a490',1,'pulsgen_ch_t']]],
  ['task_5finfinite',['task_infinite',['../structpulsgen__ch__t.html#a75b4ea125202228ed86690ae46a778c6',1,'pulsgen_ch_t']]],
  ['task_5ftoggles',['task_toggles',['../structpulsgen__ch__t.html#ab24b2c016c493b4c52a9fcf1abf24b07',1,'pulsgen_ch_t']]],
  ['task_5ftoggles_5ftodo',['task_toggles_todo',['../structpulsgen__ch__t.html#a5000d5faa5db2fd66ff0345eefd23e6c',1,'pulsgen_ch_t']]],
  ['ths_5fclk_5fdiv_5fratio_5f1',['THS_CLK_DIV_RATIO_1',['../sys_8h.html#af75c73ac00d4afcaff2020c198949e72',1,'sys.h']]],
  ['ths_5fclk_5fdiv_5fratio_5f6',['THS_CLK_DIV_RATIO_6',['../sys_8h.html#a965d98c76ad9e02deaf082314247ccff',1,'sys.h']]],
  ['ths_5fclk_5freg',['THS_CLK_REG',['../sys_8h.html#a1e80e3ac490920cf8761b6ff62592124',1,'sys.h']]],
  ['ths_5fclk_5fsclk_5fgating',['THS_CLK_SCLK_GATING',['../sys_8h.html#a9aff9c4c93ca6cbd92c243cbb6d223e6',1,'sys.h']]],
  ['ths_5fclk_5fsrc_5fosc24m',['THS_CLK_SRC_OSC24M',['../sys_8h.html#acb327f3c3edb1b719e3f8bc89e5304a3',1,'sys.h']]],
  ['ths_5fgating',['THS_GATING',['../sys_8h.html#a7f4f7a1fef4b8fab540c50af006d7722',1,'sys.h']]],
  ['ths_5frst',['THS_RST',['../sys_8h.html#a28b1a98f4c4c6976a195f57370355f8d',1,'sys.h']]],
  ['timer_5fcnt_5fget',['timer_cnt_get',['../mod__timer_8c.html#ac0b22a5df951fddf6977edf3b1cd55f6',1,'timer_cnt_get():&#160;mod_timer.c'],['../mod__timer_8h.html#ac0b22a5df951fddf6977edf3b1cd55f6',1,'timer_cnt_get():&#160;mod_timer.c'],['../mod__timer_8h.html#a8e6a3b2eb0563b3d94d83939efce5b21',1,'TIMER_CNT_GET():&#160;mod_timer.h']]],
  ['timer_5fcnt_5fget_5f64',['timer_cnt_get_64',['../mod__timer_8c.html#a197b3a4ed7085bc37f8dcb177c80486f',1,'timer_cnt_get_64():&#160;mod_timer.c'],['../mod__timer_8h.html#a197b3a4ed7085bc37f8dcb177c80486f',1,'timer_cnt_get_64():&#160;mod_timer.c']]],
  ['timer_5fcnt_5fset',['timer_cnt_set',['../mod__timer_8c.html#aef86d8f8e96209074113a6579ba042ce',1,'timer_cnt_set(uint32_t cnt):&#160;mod_timer.c'],['../mod__timer_8h.html#aef86d8f8e96209074113a6579ba042ce',1,'timer_cnt_set(uint32_t cnt):&#160;mod_timer.c'],['../mod__timer_8h.html#a6cb2b7772151124948e5d17d1c6e08ea',1,'TIMER_CNT_SET():&#160;mod_timer.h']]],
  ['timer_5ffrequency',['TIMER_FREQUENCY',['../mod__timer_8h.html#a150e4f9c8426a49d5153393b6612ef9e',1,'mod_timer.h']]],
  ['timer_5ffrequency_5fmhz',['TIMER_FREQUENCY_MHZ',['../mod__timer_8h.html#a19f140dc1376350673aaac7d35cf1ccf',1,'mod_timer.h']]],
  ['timer_5fstart',['timer_start',['../mod__timer_8c.html#a161432848a326da3a1ea7765fc2bc023',1,'timer_start():&#160;mod_timer.c'],['../mod__timer_8h.html#a161432848a326da3a1ea7765fc2bc023',1,'timer_start():&#160;mod_timer.c'],['../mod__timer_8h.html#a61d5531e2b8556916fa42733208f0659',1,'TIMER_START():&#160;mod_timer.h']]],
  ['timer_5fstop',['TIMER_STOP',['../mod__timer_8h.html#a8055a02ef3688352212e0dba357905b3',1,'TIMER_STOP():&#160;mod_timer.h'],['../mod__timer_8c.html#aa04078f48a0e8f5b6860a393ad06b535',1,'timer_stop():&#160;mod_timer.c'],['../mod__timer_8h.html#aa04078f48a0e8f5b6860a393ad06b535',1,'timer_stop():&#160;mod_timer.c']]],
  ['todo_5ftick',['todo_tick',['../structpulsgen__ch__t.html#a8877063788184b18eadf986ed313ba26',1,'pulsgen_ch_t']]],
  ['toggles',['toggles',['../structpulsgen__msg__task__setup__t.html#a8cb6815aa8abf71a064c78bcf41a383b',1,'pulsgen_msg_task_setup_t::toggles()'],['../structpulsgen__msg__toggles__t.html#a8cb6815aa8abf71a064c78bcf41a383b',1,'pulsgen_msg_toggles_t::toggles()']]],
  ['type',['type',['../structmsg__t.html#a1d127017fb298b889f4ba24752d08b8e',1,'msg_t']]]
];
