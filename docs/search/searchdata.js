var indexSectionsWithContent =
{
  0: "_abcefghilmprstuvw",
  1: "egmp",
  2: "ailmrs",
  3: "_ceghmprt",
  4: "acefghilmprstu",
  5: "m",
  6: "eghlp",
  7: "abcegimprstuvw",
  8: "ci"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerator",
  7: "Macros",
  8: "Pages"
};

