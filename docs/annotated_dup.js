var annotated_dup =
[
    [ "encoder_ch_t", "structencoder__ch__t.html", "structencoder__ch__t" ],
    [ "encoder_msg_ch_t", "structencoder__msg__ch__t.html", "structencoder__msg__ch__t" ],
    [ "encoder_msg_counts_get_t", "structencoder__msg__counts__get__t.html", "structencoder__msg__counts__get__t" ],
    [ "encoder_msg_counts_set_t", "structencoder__msg__counts__set__t.html", "structencoder__msg__counts__set__t" ],
    [ "encoder_msg_pin_setup_t", "structencoder__msg__pin__setup__t.html", "structencoder__msg__pin__setup__t" ],
    [ "encoder_msg_setup_t", "structencoder__msg__setup__t.html", "structencoder__msg__setup__t" ],
    [ "encoder_msg_state_get_t", "structencoder__msg__state__get__t.html", "structencoder__msg__state__get__t" ],
    [ "encoder_msg_state_set_t", "structencoder__msg__state__set__t.html", "structencoder__msg__state__set__t" ],
    [ "gpio_msg_port_mask_t", "structgpio__msg__port__mask__t.html", "structgpio__msg__port__mask__t" ],
    [ "gpio_msg_port_pin_t", "structgpio__msg__port__pin__t.html", "structgpio__msg__port__pin__t" ],
    [ "gpio_msg_port_t", "structgpio__msg__port__t.html", "structgpio__msg__port__t" ],
    [ "gpio_msg_state_t", "structgpio__msg__state__t.html", "structgpio__msg__state__t" ],
    [ "msg_recv_callback_t", "structmsg__recv__callback__t.html", "structmsg__recv__callback__t" ],
    [ "msg_t", "structmsg__t.html", "structmsg__t" ],
    [ "pulsgen_ch_t", "structpulsgen__ch__t.html", "structpulsgen__ch__t" ],
    [ "pulsgen_msg_ch_t", "structpulsgen__msg__ch__t.html", "structpulsgen__msg__ch__t" ],
    [ "pulsgen_msg_pin_setup_t", "structpulsgen__msg__pin__setup__t.html", "structpulsgen__msg__pin__setup__t" ],
    [ "pulsgen_msg_state_t", "structpulsgen__msg__state__t.html", "structpulsgen__msg__state__t" ],
    [ "pulsgen_msg_task_setup_t", "structpulsgen__msg__task__setup__t.html", "structpulsgen__msg__task__setup__t" ],
    [ "pulsgen_msg_toggles_t", "structpulsgen__msg__toggles__t.html", "structpulsgen__msg__toggles__t" ]
];